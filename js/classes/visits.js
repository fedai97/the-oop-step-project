class Visit {
    constructor(id ,visitName, date, fullName) {
        this.id = id.length > 4 ? id : `${id}_${Math.floor(Math.random() * (99999 + 1))}`;
        this.visitName = visitName;
        this.date = date;
        this.fullName = fullName;
    }
}

class Cardiologist extends Visit {
    constructor(id, date, fullName, visitTarget, ordinaryPressure, weightIndex, sufferedDiseases) {
        super(id, 'Кардиолог', date, fullName);
        this.visitTarget = visitTarget;
        this.ordinaryPressure = ordinaryPressure;
        this.weightIndex = weightIndex;
        this.sufferedDiseases = sufferedDiseases;
    }
}

class Dentist extends Visit {
    constructor(id, date, fullName, visitTarget, lastVisitDate) {
        super(id, 'Cтоматолог', date, fullName);
        this.visitTarget = visitTarget;
        this.lastVisitDate = lastVisitDate;
    }
}

class Therapist extends Visit {
    constructor(id, date, fullName, visitTarget, age) {
        super(id, 'Терапевт', date, fullName);
        this.visitTarget = visitTarget;
        this.age = age;
    }
}
