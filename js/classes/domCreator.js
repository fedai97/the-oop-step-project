class DOMCreator {
    static updateSelect() {
        const data = DataWorker.doctorBlankData;
        const select = document.getElementById('select');
        let optionFields = '';

        select.innerHTML = `<option value="no-doc">Выберите врача:</option>`;
        optionFields += data.map(item => `<option value="${item.id}">${item.title}</option>`);
        select.innerHTML += optionFields
    }

    static createVisitForm(value = null) {
        const data = DataWorker.doctorBlankData;
        const divNew = document.getElementById('new-inputs');
        const divForBtn = document.querySelector('.btn-submit');
        const textarea = document.querySelector('#comments');
        const eventValue = value;
        let inputsFields = '';

        if (data.find(item => item.id === eventValue)) {
            data.filter(doctor => doctor.id === eventValue)
                .forEach(item => {
                    inputsFields += item.fields.map(field => `<input type="${field.type}" id="${field.id}" placeholder="${field.title}" required>`).join('');
                });

            divNew.innerHTML = inputsFields;
            divForBtn.innerHTML = `<input type="submit" value="Запись">`
        } else {
            textarea.value = '';
            divNew.innerHTML = '';
            divForBtn.innerHTML = '';
        }
    }

    static createNote(data, comments) {
        const visitBoard = document.querySelector('.visit-board');
        const visitCard = document.createElement('div');
        const cardInf = document.createElement('div');
        const deleteCardBtn = document.createElement('button');
        const showCardInf = document.createElement('button');
        const fullInf = document.createElement('div');
        const delMessage = document.querySelector('.board-message');

        cardInf.classList.add('class', 'cardInf');
        fullInf.classList.add('class', 'fullInf');
        showCardInf.classList.add('class', 'hide');
        deleteCardBtn.classList.add('class', 'closeCard');
        visitBoard.appendChild(visitCard);
        visitCard.appendChild(cardInf);
        visitCard.appendChild(fullInf);
        visitCard.appendChild(deleteCardBtn);
        let {fullName, visitName, date, id, ...rest} = data;

        visitCard.id = id;
        visitCard.classList.add('class', 'visitCard');

        showCardInf.innerHTML = 'Показать больше';
        deleteCardBtn.innerHTML = `<i class="fa fa-times deleteCard" aria-hidden="true"></i>`;
        cardInf.innerHTML = `<p class="cardContent">${data.visitName}</p>
                              <p class="cardContent">${data.date}</p>
                              <p class="cardContent">${data.fullName}</p>`;
        visitCard.appendChild(showCardInf);
        for (let item in rest) {
            fullInf.innerHTML += `<p>${rest[item]}</p>`;
        }

        showCardInf.addEventListener('click', function () {
            showCardInf.classList.toggle('showFullInf');
            if (showCardInf.classList.contains('showFullInf')) {
                fullInf.style.display = 'block';
                showCardInf.innerHTML = 'Скрыть дополнительную информацию';
            } else {
                fullInf.style.display = 'none';
                showCardInf.innerHTML = 'Показать больше';
            }
        });

        //-------------------- DRAG AND DROP -------------------------
        cardInf.addEventListener('mousedown', function(event) {
            const visitBoardOffsetLeft = getCords(visitBoard).left;
            const visitBoardOffsetTop = getCords(visitBoard).top;

            visitCard.style.position = 'absolute';
            visitCard.style.zIndex = 9999;
            visitBoard.append(visitCard);

            moveAt(event.pageX, event.pageY);

            document.addEventListener('mousemove', onMouseMove);

            cardInf.addEventListener('mouseup', function() {
                document.removeEventListener('mousemove', onMouseMove);
                cardInf.onmouseup = null;
            });

            function moveAt(pageX, pageY) {
                if(pageX > visitBoardOffsetLeft + visitCard.offsetWidth / 2
                    && pageY > visitBoardOffsetTop + visitCard.offsetWidth / 2){
                    visitCard.style.left = pageX - visitCard.offsetWidth / 2 - visitBoardOffsetLeft + 'px';
                    visitCard.style.top = pageY - visitCard.offsetWidth / 2 - visitBoardOffsetTop + 'px';
                }
            }
            function getCords(elem) {
                const box = elem.getBoundingClientRect(),
                      body = document.body,
                      docEl = document.documentElement,
                      scrollTop = window.pageYOffset || docEl.scrollTop || body.scrollTop,
                      scrollLeft = window.pageXOffset || docEl.scrollLeft || body.scrollLeft,
                      clientTop = docEl.clientTop || body.clientTop || 0,
                      clientLeft = docEl.clientLeft || body.clientLeft || 0,
                      top = box.top + scrollTop - clientTop,
                      left = box.left + scrollLeft - clientLeft;
                return {
                    top: top,
                    left: left
                };
            }
            function onMouseMove(event) {
                moveAt(event.pageX, event.pageY);
            }
        });
        //-----------------------------------------------------


        deleteCardBtn.addEventListener('click', function () {
            const deleteTarget = this.parentNode;
            LocalStorageWorker.deleteItemInLocalStorage(deleteTarget.id);
            this.parentNode.parentElement.removeChild(this.parentNode)
        });

        if(delMessage){
            visitBoard.removeChild(delMessage);
        }
    }
}



