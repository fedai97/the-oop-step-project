class LocalStorageWorker{
    static set saveToLocalStorage(data) {
        localStorage.setItem(`${data.id}`, `${JSON.stringify(data)}`);
    }

    static deleteItemInLocalStorage(key) {
        delete localStorage[`${key}`]
    }

    static loadFromLocalStorage() {
        for(let item in localStorage){
            if(item.search('terr') !== -1 || item.search('card') !== -1 || item.search('dent') !== -1){
                const dataFromLS = JSON.parse(localStorage[item]);
                const newDoctor = DataWorker.dataProcessing(dataFromLS, item);
                DOMCreator.createNote(newDoctor, '--to do');
            }
        }
    }
}