class DataWorker {
    static get doctorBlankData() {
        return [
            {
                id: 'card',
                title: 'Кардиолог',
                fields: [
                    {title: 'Цель визита', type: 'text', id: 'visitTarget'},
                    {title: 'Обычное давление', type: 'text', id: 'ordinaryPressure'},
                    {title: 'Индекс массы тела', type: 'text', id: 'weightIndex'},
                    {title: 'Перенесенные заболевания сердечно-сосудистой системы', type: 'text', id: 'sufferedDiseases'},
                    {title: 'Возраст', type: 'number', id: 'age'},
                    {title: 'ФИО', type: 'text', id: 'fullName'}
                ]
            },
            {
                id: 'dent',
                title: 'Стоматолог',
                fields: [
                    {title: 'Цель визита', type: 'text', id: 'visitTarget'},
                    {title: 'Дата последнего посещения', type: 'date', id: 'lastVisitDate'},
                    {title: 'ФИО', type: 'text', id: 'fullName'}
                ]
            },
            {
                id: 'terr',
                title: 'Терапевт',
                fields: [
                    {title: 'Цель визита', type: 'text', id: 'visitTarget'},
                    {title: 'Возраст', type: 'number', id: 'age'},
                    {title: 'ФИО', type: 'text', id: 'fullName'}
                ]
            }
        ];
    }
    static dataProcessing (data, selectedOption){
        const date = new Date();
        let newData = {};
        let dataStr = '';

        if(!data.date){
            dataStr = `${date.getDate()}:${date.getMonth() + 1}:${date.getFullYear()}`;
        }else{
            dataStr = data.date;
        }

        if (selectedOption.split('_')[0] === 'card') {
            newData = new Cardiologist(selectedOption, dataStr, data.fullName, data.visitTarget, data.ordinaryPressure, data.weightIndex, data.sufferedDiseases);
        } else if (selectedOption.split('_')[0] === 'dent') {
            newData = new Dentist(selectedOption, dataStr, data.fullName, data.visitTarget, data.lastVisitDate);
        } else if (selectedOption.split('_')[0] === 'terr') {
            newData = new Therapist(selectedOption, dataStr, data.fullName, data.visitTarget, data.age);
        }
        return newData;
    }
}

