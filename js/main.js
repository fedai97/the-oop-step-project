document.addEventListener('DOMContentLoaded', function () {
        const select = document.getElementById('select');
        const createModal = document.querySelector('.create-btn');
        const form = document.getElementById('form-visit');
        let selectedOption = '';

        if (localStorage.length > 0) {
            LocalStorageWorker.loadFromLocalStorage();
        } else {
            const visitBoard = document.querySelector('.visit-board');
            const message = document.createElement('div');
            message.className = 'board-message';
            message.innerHTML = 'Записки не были добавлены';
            visitBoard.appendChild(message);
        }

        createModal.addEventListener('click', function () {
            const closeBtn = document.querySelector('.close');
            const modal = document.querySelector('.modalDialog');

            modal.style.display = 'block';
            DOMCreator.updateSelect();
            DOMCreator.createVisitForm();

            closeBtn.addEventListener('click', function () {
                const modal = document.querySelector('.modalDialog');
                modal.style.display = 'none';
            });
        });

        select.addEventListener('change', function () {
            selectedOption = this.value;
            DOMCreator.createVisitForm(selectedOption);
        });

        form.addEventListener('submit', function (e) {
            const modal = document.querySelector('.modalDialog');
            const inputsList = this.querySelectorAll('input:not([type=submit])');
            const textareaValue = this.querySelector('#comments').value;
            let inputsValueList = {};
            e.preventDefault();

            for (let value of inputsList) {
                inputsValueList[value.id] = value.value;
            }

            const newDoctor = DataWorker.dataProcessing(inputsValueList, selectedOption);

            DOMCreator.createNote(newDoctor, textareaValue);
            LocalStorageWorker.saveToLocalStorage = newDoctor;
            modal.style.display = 'none';
        });

    }
);