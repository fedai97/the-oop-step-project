# The OOP Step Project
*  [Андриенко Федор Олегович](https://gitlab.com/fedai97)
*  [Шпак Владислав Васильевич](https://gitlab.com/DovgushaVl)

### Андриенко Федор
- dataWorker.js → doctorBlankData, dataProcessing;
- domCreator.js → updateSelect, createVisitForm;
- localStorageWorker.js → saveToLocalStorage, deleteItemInLocalStorage, loadFromLocalStorage.

### Шпак Владислав
- dataWorker.js → createNote;
- visits.js

### Link: [OOP-Step](https://fedai97.gitlab.io/the-oop-step-project/)